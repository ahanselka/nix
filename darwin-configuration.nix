{ config, pkgs, ... }:

{
    nixpkgs.config.allowUnfree = true;

    environment.systemPackages =
    [ 

        # Shells
        pkgs.zsh
        pkgs.bash
        pkgs.fish
        pkgs.powershell

        # Editors
        pkgs.nano
        pkgs.vim

        # Unix Tools
        pkgs.ncdu
        pkgs.tmux
        pkgs.screen
        pkgs.jq
        pkgs.tree
        pkgs.htop
        pkgs.gnutar
        pkgs.gzip
        pkgs.gnupg
        pkgs.coreutils
        pkgs.findutils
        pkgs.gnused
        pkgs.openssl
        pkgs.xz
        pkgs.git
        pkgs.git-crypt
        pkgs.git-lfs
        pkgs.wget

        # Languages
        pkgs.python
        pkgs.ruby
        pkgs.go
        
        # Mac apps
        pkgs._1password
        # pkgs.iterm2
        pkgs.vscode

        # Other stuff
        pkgs.ansible
        pkgs.mosh
        pkgs.lynx

    ];

    services.nix-daemon.enable = true;

    programs.bash.enable = true;
    programs.zsh.enable = true;
    programs.fish.enable = true;

    programs.gnupg.agent = { 
        enable = true; 
        enableSSHSupport = true; 
    };

    environment.shells = with pkgs; [ bashInteractive fish zsh ];

    system.defaults.NSGlobalDomain = {
        InitialKeyRepeat = 5;
        KeyRepeat = 2;
        AppleTemperatureUnit = "Celsius";
    };

    system.keyboard.remapCapsLockToEscape = true;
    system.keyboard.enableKeyMapping = true;

    system.stateVersion = 4;

    time.timeZone = "America/Chicago";
}
