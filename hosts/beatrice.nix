{ config, pkgs, ... }:

{
    imports = [
        ../darwin-configuration.nix
    ];
    networking.hostName = "beatrice";
    # Set the screenshot directory.
    system.defaults.screencapture.location = "~/Nextcloud/Screenshots/beatrice/";
}
