{ config, pkgs, ... }:

{
    imports = [
        ../darwin-configuration.nix
    ];
    networking.hostName = "octavia";
    # Set the screenshot directory.
    system.defaults.screencapture.location = "~/Nextcloud/Screenshots/octavia/";
}
